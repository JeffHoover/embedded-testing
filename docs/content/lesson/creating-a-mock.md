---
title: "Creating a Mock"
date: 2019-03-20T22:13:23-04:00
draft: true
categories:
tags:
keywords:
---

## Declare The Interface

### Mock Function Interface

You will need to create a local version of the header file which declares the
interface you are trying to mock.  You must exactly match the function signature.

This is necessary because heder files for embedded libraries tend to contain
references to things you can't or don't want to include in tests that you're
going to run on a developer workstation.

I usually include these files in a folder called `system` in the root of my
source tree.  In my test folder's Makefile I include this folder as an include
search path.

``` C
// Arduino.h
#ifndef _ARDUINO_H_
#define _ARDUINO_H_

void pinMode(uint8_t pin, uint8_t mode);

#endif
```

### Control Interface

In addition to the function being mocked, we'll need to declare the functions
that we'll use to interact with the mock.  These include functions to set up
return values for the mock, and functions to test what values the mock was
called with.

I put the mocked in a folder `mock` in the root folder of my project.  The
Makefile for the tests lists this folder as an include path.  It also lists
this folder as a linker path to link with the library build from the mocks.

``` C
// mock_pinmode.h
#ifndef _MOCK_PINMODE_H_
#define _MOCK_PINMODE_H_

#include <stdbool.h>

bool pinMode_called_with(uint8_t candidatePin, uint8_t candidateMode);

#endif
```

## Implementing The Mock

What you implement for your mock will depend on what you need from the mock.  In the case of `pinMode`, we need to record the parameters it was called with so we can test them later.  We have at least these objectives:

  1. Declare variables for the parameters in a scope available to our `pinMode` implementation, as well as our `pinMode_called_with` function.  The values need to persist after the function has returned.
  1. Record the values passed to `pinMode` in the static variables.
  1. Return true if `pinMode_called_with` is called with the same parameters as `pinMode`

``` C
#include "Arduino.h"
#include <stdbool.h>
#include <stdint.h>

static uint8_t actual_pin = 0;
static uint8_t actual_mode = 0;

void pinMode(uint8_t pin, uint8_t mode) {
  actual_pin = pin;
  actual_mode = mode;
}

bool pinMode_called_with(uint8_t pin, uint8_t mode) {
  return pin == actual_pin && mode == actual_mode;
}
```
