---
title: Embedded Testing
---

# Welcome!

The Embedded Testing workshop is a way to try out Test First development for embedded software.

The workshop takes two forms:

1. Half day, for teams familiar with embedded development or test-first development.
1. All day, for teams new to embedded development and test-first development.
